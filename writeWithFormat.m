//[ViewController writeWithFormat:@"1", @"2", @"3", nil];
+ (void)__methodName__:(NSObject*)string, ... {// parms must be end with nil
    va_list args;
    va_start(args, string);
    if (string) {
        NSLog(@"Do something with First: %@", string);
        NSObject *other;
        while ((other = va_arg(args, NSObject *))) {
            NSLog(@"Do something with other: %@", other);
        }
    }
    va_end(args);
}